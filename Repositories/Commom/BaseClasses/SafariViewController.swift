import UIKit
import SafariServices

class SafariViewController: SFSafariViewController {

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }

}
